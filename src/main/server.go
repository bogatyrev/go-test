// Hash server
package main

import (
	"strconv"

	"net/http"
	"encoding/json"
	"log"
	"errors"

	"os/signal"
	"os"
	"context"

	"database/sql"
	_ "github.com/jackc/pgx/stdlib"

	"github.com/joho/godotenv"
)

// Minimum rounds per request
const minHashRounds = 1

// Task statuses 
const (
	TaskInProgress string = "InProgress"
	TaskComplete string = "Complete"
	TaskFailed string = "Failed"
)

// HashRequest request json data
type HashRequest struct {
	Payload string			`json:"payload"`
	HashRounds int 			`json:"hash_rounds_cnt"`
}

// Validate request values
func (d *HashRequest) Validate() error {

	if len(d.Payload) == 0 {
		return errors.New("payload is required")
	}

	if (d.HashRounds < minHashRounds) {
		return errors.New("Value of hash rounds must be grater than 0")
	}

	return nil
}

// HashResponse represents json struct for task id
type HashResponse struct {
	Id int	`json:"id"`
}

// ErrorResponse common error json struct
type ErrorResponse struct {
	Message string		`json:"message"`
}

// Handle for hash request
//
// request: POST /api/v1/status
// `{
//    "payload": "sample",
//    "hash_rounds_cnt": 3
// }`
// response:
// `{
//    "id": 1234,
// }`
//
// error:
//`{"message": "some error ..."}`
func hashHandle(w http.ResponseWriter, r *http.Request) {

	if r.Method != http.MethodPost {
		sendError(w, "Method not allowed", 405)
		return
	}

	req := &HashRequest{}

	if 	err := json.NewDecoder(r.Body).Decode(req); err != nil {
		sendError(w, err.Error(), 500)
		return
	}

	if err := req.Validate(); err != nil {
		sendError(w, err.Error(), 500)
		return
	}

	task := &Task{
		SourceValue: req.Payload, 
		HashRounds: req.HashRounds,
		Status: TaskInProgress,
	}
	
	if err := taskRepo.Save(task); err != nil {
		sendError(w, err.Error(), 500)
		return
	}

	// run generation
	go execute(task)

	resp := &HashResponse{Id: task.Id}
	respBytes, err := json.Marshal(resp)
	if err != nil {
		sendError(w, err.Error(), 500)
		return
	}

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.Write(respBytes)
}

// Handle for hash status request
//
// request: GET /api/v1/status?id=%ID%
//
// response:
// `{
//    "id": 1234
//    "payload": "sample",
//    "hash_rounds_cnt": 3,
//    "status": "complete",
//    "hash": "123hf33a",
// }`
//
// error:
//`{"message": "some error ..."}`
func statusHandle(w http.ResponseWriter, r *http.Request) {

	if r.Method != http.MethodGet {
		sendError(w, "Method not allowed", 405)
		return
	}

	taskParam := r.URL.Query().Get("id")

	taskID, err := strconv.Atoi(taskParam)
	if 	err != nil {
		sendError(w, err.Error(), 500)
		return
	}

	task, err := taskRepo.FindByID(taskID)
	if err != nil {
		sendError(w, err.Error(), 500)
		return
	}

	respBytes, err := json.Marshal(task)
	if err != nil {
		sendError(w, err.Error(), 500)
		return
	}

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.Write(respBytes)
}

// Executes task
func execute(t *Task) {
	t.Hash = calculateHash(t.SourceValue, t.HashRounds)
	t.Status = TaskComplete

	if err := taskRepo.Save(t); err != nil {
		log.Print(err)
	}
}

// sendError writes json erro
func sendError(w http.ResponseWriter, error string, code int) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(code)
	resp := &ErrorResponse{Message: error}
	respBytes, err := json.Marshal(resp)
	if err != nil {
		log.Print(err)
	}
	w.Write(respBytes)
}

var taskRepo TaskRepository

func main() {
	err := godotenv.Load()
	if err != nil {
	  log.Fatal("Error loading .env file")
	}

	Db, err := sql.Open("pgx", createDSN())
	if err != nil {
		log.Fatal(err)
	}
	defer Db.Close()

	if err := Db.Ping(); err != nil {
		log.Fatal(err)
	}
	taskRepo.Db = Db

	portNumber := os.Getenv("SERVER_PORT")
	http.HandleFunc("/v1/hash", hashHandle)
	http.HandleFunc("/v1/status", statusHandle)

	api := &http.Server{Addr: ":"+portNumber, Handler: nil}

	idleConnsClosed := make(chan struct{})
	go func() {
		sigint := make(chan os.Signal, 1)
		signal.Notify(sigint, os.Interrupt)
		<-sigint

		if err := api.Shutdown(context.Background()); err != nil {
			log.Printf("HTTP server Shutdown: %v", err)
		}
		close(idleConnsClosed)
	}()

	if err := api.ListenAndServe(); err != http.ErrServerClosed {
		log.Fatalf("HTTP server ListenAndServe: %v", err)
	}

	<-idleConnsClosed
}
