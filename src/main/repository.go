
package main

import (
	"errors"
	"database/sql"
	_ "github.com/jackc/pgx/stdlib"
)

// Task in databse
type Task struct {
	Id int				`json:"id"`
	SourceValue string  `json:"payload"`
	HashRounds int		`json:"hash_rounds_cnt"`
	Status string		`json:"status"`
	Hash string			`json:"hash"`
}

// TaskRepository for *Task
type TaskRepository struct {
	Db *sql.DB
}

// FindByID returns *Task entity
func (r TaskRepository) FindByID(id int) (*Task, error) {
	res := &Task{}
	stmt, err := r.Db.Prepare("SELECT id, src, rounds, status, hash FROM task WHERE id = $1")
	if err != nil {
		return nil, err
	}
	defer stmt.Close()

	err = stmt.QueryRow(id).Scan(&res.Id, &res.SourceValue, &res.HashRounds, &res.Status, &res.Hash)
	if  err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
		return nil, err
	}

	return res, nil
}

// Save inserts or update *Task
func (r TaskRepository) Save(task *Task) error {
	if task == nil {
		return errors.New("nil not allowed")
	}
	if task.Id == 0 {
		if err := r.insert(task); err != nil {
			return err
		}
 	} else {
		if err := r.update(task); err != nil {
			return err
		}
	}

	return nil
}

func (r TaskRepository) insert(task *Task) error {
	stmt, err := r.Db.Prepare("INSERT INTO task (src, rounds, status, hash) VALUES ($1,$2,$3,$4) RETURNING id")
	if err != nil {
		return err
	}
	defer stmt.Close()

	var lastInsertedID int
	err = stmt.QueryRow(task.SourceValue, task.HashRounds, task.Status, task.Hash).Scan(&lastInsertedID)
	if err != nil {
		return err
	}
	task.Id = lastInsertedID

	return nil
}

func (r TaskRepository) update(task *Task) error {
	stmt, err := r.Db.Prepare("UPDATE task SET src=$1, rounds=$2, status=$3, hash=$4 WHERE id=$5")
	if err != nil {
		return err
	}
	defer stmt.Close()

	_, err = stmt.Exec(task.SourceValue, task.HashRounds, task.Status, task.Hash, task.Id)
	if err != nil {
		return err
	}

	return nil
}