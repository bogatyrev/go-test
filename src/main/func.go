
package main

import (
	"os"
	"fmt"
	"crypto/sha256"
)

// Calculate hash value of string
func calculateHash(source string, rounds int) string {
	var data []byte = []byte(source)

	for i := 0; i < rounds; i++ {
		res := sha256.Sum256(data)
		data = res[:]
	}

	return fmt.Sprintf("%x", data)
}

// Formats DSN string
func createDSN() string {
	user := os.Getenv("POSTGRES_USER")
	password := os.Getenv("POSTGRES_PASS")
	host := os.Getenv("POSTGRES_HOST")
	port := os.Getenv("POSTGRES_PORT")
	dbName := os.Getenv("POSTGRES_DB")
	sslMode := os.Getenv("POSTGRES_SSLMODE")

	return fmt.Sprintf(
		"user=%s password=%s host=%s port=%s database=%s sslmode=%s", 
		user,
		password,
		host,
		port,
		dbName,
		sslMode,
	)
}