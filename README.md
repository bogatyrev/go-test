# Go Developer - Тестовое задание

Легенда
-------

Вы выполняете заказ для английской компании и хотите показать себя и компанию, в которой работаете, с самой лучшей стороны. Планируется, что заниматься деплоем и поддержкой сервиса после сдачи будут другие специалисты.
Задача

Хэширование - процесс необратимого преобразования входного текста произвольной длины в псевдослучайный текст фиксированной длины.

Хэширование используется в самых различных задачах, например, для того чтобы на основе пользовательского пароля произвести более безопасный ключ шифрования каких-либо данных. Для этой цели хэширование производится многократно (например, по стандарту PKCS #5).

Задача заключается в том чтобы разработать простой публичный сервис многократного sha2-хэширования. При запуске сервис должен начать слушать определенный порт и принимать запросы.

# Эндпоинты

Все запросы и ответы должны быть в формате JSON.
Отправка задачи на хэширование

1. Пользователь отправляет запрос, содержащий задачу на    хэширование с полями:
a. payload - хэшируемый текст;
b. hash_rounds_cnt - количество раундов хэширования.
2. Сервис сохраняет задачу в базу и мгновенно выдает id задачи на хэширование
пользователю.
3. Сервис в фоне сразу же стартует процесс хэширования и по результату
записывает результат в базу.
Например, если пользователь отправил payload=”the answer is 42” и hash_rounds_cnt=3, то результатом задачи на хэширование должен быть sha2(sha2(sha2(“the answer is 42”))).

# Получение результата

Используя полученный id пользователь может получить результат этой задачи с полями:

1. id задачи.
2. payload - передана при создании задачи.
3. hash_rounds_cnt - была передана при создании задачи.
4. status - статус задачи:
a. “in progress” - еще в процессе вычислений;
b. “finished” - завершена.
5. hash - результат хэширования.
Результаты хэширования по выданным id не должны уничтожаться между перезапусками приложения.
Требования
1. Сервис должен быть разработан с использованием Go и PostgreSQL и должен функционировать корректно. Для локального запуска для удобства можно использовать Docker Compose.
2. Конфигурация сервиса (например, параметры подключения к БД) должна лежать отдельно от самого сервиса.
3. Можно использовать любые сторонние библиотеки, упрощающие разработку.
4. Должно быть описана краткая документация API
5. На всем протяжении работы над сервисом необходимо использовать git.
Рекомендуем вести разработку таким образом, что на каждом этапе выделять наиболее важные требования и двигаться от них к менее важным.
Репозиторий с исходным кодом тестового задания должен быть запушен на bitbucket.org, а ссылка на него отправлена в ответном письме.